const table = createTable();
document.body.append(table);

function createTable() {
	let fragment = document.createDocumentFragment();
	let table = document.createElement('table');
	fragment.appendChild(table);
	for(let i = 0; i < 30; i++) {
		let newRow = document.createElement('tr');
		table.appendChild(newRow);		
		for(let j = 0; j < 30; j++) {
			let newData = document.createElement('td');
			newRow.appendChild(newData);
		}
	}
	return table;
}

document.addEventListener('click', function(event) {
	let target = event.target;

	if(target.tagName === 'BODY'){
		table.classList.toggle('black-table');
		let activeDatas = document.querySelectorAll('.black-item');
		activeDatas.forEach(e => e.classList.toggle('white-item'));
	}	
	else {
		while(target !== table){
			if(target.tagName === "TD") {
				target.classList.toggle('black-item');
			}
			target = target.parentNode;
		}
	}
});